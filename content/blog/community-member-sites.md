---
title: "Community Member Sites & Projects"
date: 2022-04-06T11:56:20+05:30
draft: false
---

[gtlsgamr (IRC Operator)](https://hitarththummar.xyz/)
- IRC: gtlsgamr @ libera.chat and irc.ircnow.org

[AryaK (Matrix admin)](https://aryak.codeberg.page/)
- [matrix: @arya:envs.net](https://matrix.to/#/@arya:envs.net)

[NeoVoid (Telegram admin)](https://neovoid.tk/)
- [tg: @pineapples5972](https://telegram.me/pineapples5972)

[PyStarDust](https://harshith.xyz/)
- IRC: pystardust @ libera.chat
- Worth Mentioning: Projects [ani-cli](https://github.com/pystardust/ani-cli), [ytfzf](https://github.com/pystardust/ytfzf), 

[bugswriter](https://bugswriter.com/)
- Worth Mentioning: [on YouTube: BugsWriter0x1337](https://www.youtube.com/c/BugsWriter0x1337)
- Worth Mentioning: Projects [notflix](https://github.com/Bugswriter/notflix), [tuxi](https://github.com/Bugswriter/tuxi)

[relejek](https://relejek.ml)
- [matrix: @relejek:matrix.org](https://matrix.to/#/@relejek:matrix.org)

[daksh](https://daksh.tilde.team/)
- [matrix: @daksh:tchncs.de](https://matrix.to/#/@daksh:tchncs.de)

[ramram](https://jumpher.github.io/gotakhor)

[abhi](https://abhishrijoshi.xyz/) : site-down

