---
title: "How to contribute to the website"
date: 2021-12-20T22:19:10+05:30
draft: false
---

As mentioned in the [Welcome Post]({{< ref "/blog/welcome" >}}), most
articles in the website are meant to be made by the community

If you want to contribute, fork the website to your codeberg account and then clone the repo 

Now create a branch for your changes. 

![Git branches](/images/contributing/git-branch.png)

Now you can commit & push the changes to your fork.

![Git Add, Commit & Push](/images/contributing/git-acp.png)

From here you just need to submit a Pull Request to the Main repository!

![Creating PR 1/2](/images/contributing/ui-pr-create.png)

![Creating PR 2/2](/images/contributing/ui-pr.png)

Thats it! Thanks for Contributing to GNU/Linux India!
