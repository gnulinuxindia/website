#!/bin/sh
export red="\033[1;31m"
export green="\033[1;32m"
export yellow="\033[1;33m"
export blue="\033[1;34m"
export purple="\033[1;35m"
export cyan="\033[1;36m"
export grey="\033[0;37m"
export reset="\033[m"
export WEBSITE_DIR=${WEBSITE_DIR:-$PWD}
if [ ! -n "$(ls -A "${WEBSITE_DIR}"/public/)" ]; then
  echo Submodule not cloned...
  echo Cloning and setting up submodules
  git -C "${WEBSITE_DIR}" submodule init
  git -C "${WEBSITE_DIR}" submodule update
fi
git -C "${WEBSITE_DIR}"/public pull > /dev/null 2>&1 # Check for new commits on public/ from elsewhere
read -p "Commit message: " COMMIT_MESSAGE
read -p "commit description (leave blank for none): " COMMIT_DESC
git -C "${WEBSITE_DIR}" status
read -p "are you sure you want to commit the following changes [Y/n]: " COMMIT
case $COMMIT in
	[Yy]|[Yy][Ee][Ss]|"")
    continue
		;;
	[Nn]|[Nn][Oo])
		echo add correct files and exec the script again
    exit 1
		;;
	* )	
		echo "$2"
		;;
esac
hugo
echo Commiting to public
git -C "${WEBSITE_DIR}"/public add -A
git -C "${WEBSITE_DIR}"/public commit -a -m "${COMMIT_MESSAGE}" -m "${COMMIT_DESC}"
git -C "${WEBSITE_DIR}"/public push
echo Commiting to source
git -C "${WEBSITE_DIR}" commit -m "${COMMIT_MESSAGE}" -m "${COMMIT_DESC}"
git -C "${WEBSITE_DIR}" add public # Add the new commits if they exist
git -C "${WEBSITE_DIR}" push
printf "\n $green Done, it will be deployed to your codeberg page in sometime. $reset \n"
# The read thing isnt working correctly idk why
#printf "$green Until then you can view the website in localhost:1313 $reset \n"
#hugo server -D > /dev/null 2>&1 & read -n 1 -r -s -p $'Press any key to exit...\n' && kill $(pidof hugo)
